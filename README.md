# Trip sorter

All the bash-commands below have been prefixed with either `$` or `#` depending 
on whether the command requires root privileges or not (respectively).

## Requirements

The following are the package-names for running the tests on Ubuntu/Debian system:

* php7.1
* php7.1-mbstring

``` bash
$ apt install php7.1 php7.1-mbstring 
```

## Setting up

To set up the composer dependencies please run:

``` bash
# composer install
```

## Running tests

To run testsuite type in the following command:

``` bash
# ./vendor/bin/phpunit tests/
```

## Documentation

As phpdoc does not support PHP7.1 (yet) I did not use phpdocumentor for this.

`Strobotti\TripSorter\TripSorter::sort()` takes in an array of `Strobotti\BoardingCard\BoardingCardInterface` 
that are returned in correct order.
 
There are several implementations of `BoardingCardInterface` which are constructed with appropriate parameters 
and some have optional properties (such as `FlightBoardingCard` has optional `ticketCounterId`).

All boarding card types support `__toString()` for generating human-readable presentation.

The sorting is done using linked list that has a linear runtime or `O(n)` complexity.
