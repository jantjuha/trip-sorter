<?php

namespace Tests\Strobotti\TripSorter;

use PHPUnit\Framework\TestCase;
use Strobotti\BoardingCard\AirportBusBoardingCard;
use Strobotti\BoardingCard\FlightBoardingCard;
use Strobotti\BoardingCard\TrainBoardingCard;
use Strobotti\Location\Location;
use Strobotti\TripSorter\TripSorter;

class TripSorterTest extends TestCase
{
    static $testLocations = [];

    private function getLocationById($id)
    {
        $testLocations = [
            1 => 'Madrid',
            2 => 'Barcelona',
            3 => 'Gerona Airport',
            4 => 'Stockholm',
            5 => 'New York JFK',
        ];

        if (false === isset(TripSorterTest::$testLocations[$id])) {
            TripSorterTest::$testLocations[$id] = new Location($id,$testLocations[$id]);
        }

        return TripSorterTest::$testLocations[$id];
    }

    public function testSort()
    {
        $cards = [];

        $cards[] = (new FlightBoardingCard($this->getLocationById(3), $this->getLocationById(4), 'SK455', '45B', '3A'))
            ->setTicketCounterId(344);

        $cards[] = new TrainBoardingCard($this->getLocationById(1), $this->getLocationById(2), '78A', '45B');

        $cards[] = new AirportBusBoardingCard($this->getLocationById(2), $this->getLocationById(3));

        $cards[] = new FlightBoardingCard($this->getLocationById(4), $this->getLocationById(5), 'SK22', '22', '7B');

        $sorter = new TripSorter();

        // shuffle 10 times just to be sure ;)
        for ($i = 0; $i < 10; $i++) {
            shuffle($cards);
            $sorted = $sorter->sort($cards);

            $this->assertEquals(1, $sorted[0]->getOriginLocation()->getId());
            $this->assertEquals(2, $sorted[1]->getOriginLocation()->getId());
            $this->assertEquals(3, $sorted[2]->getOriginLocation()->getId());
            $this->assertEquals(4, $sorted[3]->getOriginLocation()->getId());
        }
    }
}
