<?php

namespace Tests\Strobotti\BoardingCard;

use PHPUnit\Framework\TestCase;
use Strobotti\BoardingCard\FlightBoardingCard;
use Strobotti\Location\Location;

class FlightBoardingCardTest extends TestCase
{
    public function testToStringWithTicketCounterId()
    {
        $origin = new Location(1, "Gerona Airport");
        $destination = new Location(2, "Stockholm");

        $card = new FlightBoardingCard($origin, $destination, 'SK455', '45B', '3A');
        $card->setTicketCounterId(344);

        $this->assertEquals('From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. Baggage drop at ticket counter 344.', "$card");
    }

    public function testToStringWithoutTicketCounterId()
    {
        $origin = new Location(1, "Stockholm");
        $destination = new Location(2, "New York JFK");

        $card = new FlightBoardingCard($origin, $destination, 'SK22', '22', '7B');

        $this->assertEquals('From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.', "$card");
    }
}
