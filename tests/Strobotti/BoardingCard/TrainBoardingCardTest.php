<?php

namespace Tests\Strobotti\BoardingCard;

use PHPUnit\Framework\TestCase;
use Strobotti\BoardingCard\TrainBoardingCard;
use Strobotti\Location\Location;

class TrainBoardingCardTest extends TestCase
{
    public function testToString()
    {
        $origin = new Location(1, "Madrid");
        $destination = new Location(2, "Barcelona");

        $card = new TrainBoardingCard($origin, $destination, '78A', '45B');

        $this->assertEquals('Take train 78A from Madrid to Barcelona. Sit in seat 45B.', "$card");
    }
}
