<?php

namespace Tests\Strobotti\BoardingCard;

use PHPUnit\Framework\TestCase;
use Strobotti\BoardingCard\AirportBusBoardingCard;
use Strobotti\Location\Location;

class AirportBusBoardingCardTest extends TestCase
{
    public function testToString()
    {
        $origin = new Location(1, "Barcelona");
        $destination = new Location(2, "Gerona Airport");

        $card = new AirportBusBoardingCard($origin, $destination);

        $this->assertEquals('Take the airport bus from Barcelona to Gerona Airport. No seat assignment.', "$card");
    }
}
