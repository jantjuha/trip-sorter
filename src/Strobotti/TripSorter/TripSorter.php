<?php

namespace Strobotti\TripSorter;

use Strobotti\BoardingCard\BoardingCardInterface;

/**
 * Class TripSorter
 *
 * A stateless sorter for sorting boarding cards based on the locations on them.
 *
 * @package Strobotti\TripSorter
 */
class TripSorter
{
    /**
     * Returns given array of boarding cards in correct order.
     *
     * @param  BoardingCardInterface[] $boardingCards
     * @return BoardingCardInterface[]
     */
    public function sort(array $boardingCards):array
    {
        /* @var $cardsByOriginId \Strobotti\BoardingCard\BoardingCardInterface[] */
        $cardsByOriginId = []; // id => card
        $startOriginCandidateIds = [];

        foreach ($boardingCards as $boardingCard) {
            $originLocationId = $boardingCard->getOriginLocation()->getId();
            $destinationLocationId = $boardingCard->getDestinationLocation()->getId();

            $cardsByOriginId[$originLocationId] = $boardingCard;

            if (false === isset($cardsByOriginId[$destinationLocationId])) {
                $startOriginCandidateIds[$originLocationId] = true;
            } elseif (isset($startOriginCandidateIds[$originLocationId])) {
                unset($startOriginCandidateIds[$originLocationId]);
            }
        }

        $startOriginId = reset($startOriginCandidateIds);

        $ret = [];

        do {
            $ret[] = $cardsByOriginId[$startOriginId];

            $startOriginId = $cardsByOriginId[$startOriginId]->getDestinationLocation()->getId();
        } while(isset($cardsByOriginId[$startOriginId]));

        return $ret;
    }
}
