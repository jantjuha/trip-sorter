<?php

namespace Strobotti\BoardingCard;

use Strobotti\Location\Location;

/**
 * Interface BoardingCardInterface
 *
 * @package Strobotti\BoardingCard
 */
interface BoardingCardInterface
{
    /**
     * @return Location
     */
    public function getOriginLocation();

    /**
     * @return Location
     */
    public function getDestinationLocation();
}
