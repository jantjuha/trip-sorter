<?php

namespace Strobotti\BoardingCard;

/**
 * Class AirportBusBoardingCard
 *
 * @package Strobotti\BoardingCard
 */
class AirportBusBoardingCard extends AbstractBoardingCard
{
    public function __toString()
    {
        return sprintf(
            'Take the airport bus from %s to %s. No seat assignment.',
            $this->getOriginLocation()->getName(),
            $this->getDestinationLocation()->getName()
        );
    }
}
