<?php

namespace Strobotti\BoardingCard;

use Strobotti\Location\Location;

/**
 * Class AbstractBoardingCard
 *
 * @package Strobotti\BoardingCard
 */
abstract class AbstractBoardingCard implements BoardingCardInterface
{
    /**
     * @var Location
     */
    private $originLocation;

    /**
     * @var Location
     */
    private $destinationLocation;

    public function __construct(Location $originLocation, Location $destinationLocation)
    {
        $this->setOriginLocation($originLocation);
        $this->setDestinationLocation($destinationLocation);
    }

    /**
     * @param Location $originLocation
     */
    public function setOriginLocation(Location $originLocation)
    {
        $this->originLocation = $originLocation;
    }

    /**
     * @return Location
     */
    public function getOriginLocation():Location
    {
        return $this->originLocation;
    }

    /**
     * @param Location $destinationLocation
     */
    public function setDestinationLocation(Location $destinationLocation)
    {
        $this->destinationLocation = $destinationLocation;
    }

    /**
     * @return Location
     */
    public function getDestinationLocation():Location
    {
        return $this->destinationLocation;
    }

    public function __toString()
    {
        return sprintf('Go from %s to %s.', $this->getOriginLocation()->getName(), $this->getDestinationLocation()->getName());
    }
}