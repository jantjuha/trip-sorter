<?php

namespace Strobotti\BoardingCard;

use Strobotti\Location\Location;

/**
 * Class FlightBoardingCard.
 *
 * @package Strobotti\BoardingCard
 */
class FlightBoardingCard extends AbstractBoardingCard
{
    /**
     * @var string
     */
    private $flightNumber;

    /**
     * @var string
     */
    private $seatNumber;

    /**
     * @var string
     */
    private $gateNumber;

    /**
     * @var int
     */
    private $ticketCounterId;

    public function __construct(Location $originLocation, Location $destinationLocation, string $flightNumber,
                                string $gateNumber, string $seatNumber
    ) {
        parent::__construct($originLocation, $destinationLocation);

        $this->setFlightNumber($flightNumber);
        $this->setGateNumber($gateNumber);
        $this->setSeatNumber($seatNumber);
    }

    /**
     * @return string
     */
    public function getFlightNumber(): string
    {
        return $this->flightNumber;
    }

    /**
     * @param string $flightNumber
     * @return FlightBoardingCard
     */
    public function setFlightNumber(string $flightNumber): FlightBoardingCard
    {
        $this->flightNumber = $flightNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getGateNumber(): string
    {
        return $this->gateNumber;
    }

    /**
     * @param string $gateNumber
     * @return FlightBoardingCard
     */
    public function setGateNumber(string $gateNumber): FlightBoardingCard
    {
        $this->gateNumber = $gateNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeatNumber(): string
    {
        return $this->seatNumber;
    }

    /**
     * @param string $seatNumber
     * @return FlightBoardingCard
     */
    public function setSeatNumber(string $seatNumber): FlightBoardingCard
    {
        $this->seatNumber = $seatNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getTicketCounterId(): ?int
    {
        return $this->ticketCounterId;
    }

    /**
     * @param int $ticketCounterId
     * @return FlightBoardingCard
     */
    public function setTicketCounterId(?int $ticketCounterId): FlightBoardingCard
    {
        $this->ticketCounterId = $ticketCounterId;

        return $this;
    }

    public function __toString()
    {
        $string = sprintf(
            'From %s, take flight %s to %s. Gate %s, seat %s.',
            $this->getOriginLocation()->getName(),
            $this->getFlightNumber(),
            $this->getDestinationLocation()->getName(),
            $this->getGateNumber(),
            $this->getSeatNumber()
        );

        // TODO add a bit more "semantic" way of determining this, in the meantime MVP will do!
        if ($this->getTicketCounterId()) {
            $string .= sprintf(' Baggage drop at ticket counter %d.', $this->getTicketCounterId());
        } else {
            $string .= ' Baggage will we automatically transferred from your last leg.';
        }

        return $string;
    }
}
