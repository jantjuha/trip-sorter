<?php

namespace Strobotti\BoardingCard;

use Strobotti\Location\Location;

/**
 * Class TrainBoardingCard
 *
 * @package Strobotti\BoardingCard
 */
class TrainBoardingCard extends AbstractBoardingCard
{
    /**
     * @var string
     */
    private $routeNumber;

    /**
     * @var string
     */
    private $seatNumber;

    /**
     * TrainBoardingCard constructor.
     *
     * @param Location $originLocation
     * @param Location $destinationLocation
     * @param string $routeNumber
     * @param string $seatNumber
     */
    public function __construct(Location $originLocation, Location $destinationLocation, string $routeNumber, string $seatNumber)
    {
        parent::__construct($originLocation, $destinationLocation);

        $this->setRouteNumber($routeNumber);
        $this->setSeatNumber($seatNumber);
    }

    /**
     * @return string
     */
    public function getRouteNumber(): string
    {
        return $this->routeNumber;
    }

    /**
     * @param string $routeNumber
     * @return TrainBoardingCard
     */
    public function setRouteNumber(string $routeNumber): TrainBoardingCard
    {
        $this->routeNumber = $routeNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeatNumber(): string
    {
        return $this->seatNumber;
    }

    /**
     * @param string $seatNumber
     * @return TrainBoardingCard
     */
    public function setSeatNumber(string $seatNumber): TrainBoardingCard
    {
        $this->seatNumber = $seatNumber;

        return $this;
    }

    public function __toString()
    {
        return sprintf(
            'Take train %s from %s to %s. Sit in seat %s.',
            $this->getRouteNumber(),
            $this->getOriginLocation()->getName(),
            $this->getDestinationLocation()->getName(),
            $this->getSeatNumber()
        );
    }
}
