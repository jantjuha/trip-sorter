<?php

namespace Strobotti\Location;

/**
 * Class Location.
 *
 * @package Strobotti\Location
 */
class Location
{
    /**
     * The id of the location.
     *
     * @var int
     */
    private $id;

    /**
     * The name of the location.
     *
     * @var string
     */
    private $name;

    /**
     * Location constructor.
     *
     * @param int $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->setId($id);
        $this->setName($name);
    }

    /**
     * Gets id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Sets id.
     *
     * @param  int $id
     * @return Location
     */
    public function setId(int $id): Location
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets name.
     *
     * @param string $name
     * @return Location
     */
    public function setName(string $name): Location
    {
        $this->name = $name;

        return $this;
    }
}